# Run

- From console: ``sbt run``
- From Intellij: Import project as sbt project and add a "Play 2 App" run configuration with the imported module

# Endpoints
- Healthcheck: GET /health-check 
- Swagger UI: GET /
- all images: GET /all
- search image: GET /search/{searchTerm}
- refresh catalog: POST /refresh-catalog