import com.google.inject.AbstractModule
import play.api.libs.concurrent.AkkaGuiceSupport
import service.ImagesService

class Module extends AbstractModule with AkkaGuiceSupport {
  override def configure() = {
    bindActor[ImagesService.CatalogRefresherActor]("imagesCatalogRefresherActor")
  }
}