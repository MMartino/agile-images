package clients

import clients.RestClient.RestException
import clients.models.{AuthRequest, AuthResponse, ImageInfo, ImagesCatalogResponse}
import com.google.inject.Inject
import play.api.Configuration
import play.api.libs.ws.{WSClient, WSRequest}
import play.cache.AsyncCacheApi
import utils.conversions.Lifting.LiftingOps

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.FutureConverters._

class AgileClient @Inject()(config: Configuration,
                            cache: AsyncCacheApi,
                            protected val ws: WSClient)(implicit val ec: ExecutionContext) extends RestClient {
  private val tokenCacheKey = "token"
  private val apiKey: String = s"${config.get[String]("agile.apiKey")}"
  private val endpoint: String = s"${config.get[String]("agile.endpoint")}"

  def retrieveImagesCatalog(page: Int = 1, limit: Int = 100): Future[ImagesCatalogResponse] = getImages(None).flatMap {
    _.withQueryStringParameters("page" -> s"$page", "limit" -> s"$limit")
      .exec[ImagesCatalogResponse](GET)
      .recoverWith { case e: RestException if e.isUnathorized =>
        logger.info(s"Refreshing expired token and retrying..", e)
        authenticate().flatMap(_ => retrieveImagesCatalog(page, limit))
      }
  }

  def retrieveImageInfo(id: String): Future[Option[ImageInfo]] = getImages(Some(id)).flatMap {
    _.exec[ImageInfo](GET)
      .map(_.liftF[Option])
      .recover { case e: RestException if e.is404 =>
        logger.warn(s"Ignoring invalid id $id", e)
        None
      }
  }

  private def getToken(): Future[String] = cache.getOrElseUpdate(tokenCacheKey, () => authenticate().asJava).asScala

  private def authenticate(): Future[String] = ws
    .url(s"$endpoint/auth")
    .exec[AuthResponse](POST(AuthRequest(apiKey)))
    .map(_.token)
    .flatMap(token => cache.set(tokenCacheKey, token).asScala.map(_ => token))

  private def getImages(subPath: Option[String]): Future[WSRequest] = getToken().map { token =>
    ws
      .url(s"$endpoint/images/${subPath.getOrElse("")}")
      .withHttpHeaders("Accept" -> "application/json", "Authorization" -> s"Bearer $token")
  }
}