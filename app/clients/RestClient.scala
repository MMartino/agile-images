package clients

import clients.RestClient._
import play.api.Logging
import play.api.libs.json.Json.toJson
import play.api.libs.json.{JsValue, Reads, Writes}
import play.api.libs.ws.{WSClient, WSRequest, WSResponse}
import utils.extensions.AsyncExtensions.FutureOps
import utils.extensions.JsonExtensions.JsValueExtensions
import utils.extensions.ScopingFunctions
import utils.extensions.StringExtensions.StringOps
import utils.noop

import scala.concurrent.{ExecutionContext, Future}
import scala.language.implicitConversions

trait RestClient extends Logging {
  protected val ws: WSClient
  implicit val ec: ExecutionContext

  implicit class WSRequestExtesions(request: WSRequest) {
    def exec[Response: Reads](requestType: RequestType, callback: WSResponse => _ = noop): Future[Response] = (requestType match {
      case GET => request.get()
      case DELETE => request.delete()
      case PUT(body) => request.put(body)
      case POST(body) => request.post(body)
      case PATCH(body) => request.patch(body)
    })
      .also(_.onComplete(_.fold(logger.error(s"Call to '${requestType.method}: ${request.url}'  failed", _), callback)))
      .parse[Response](DetailedRestException(request, requestType.method, requestType.body, _))
  }

  implicit class WSResponseExtensions(response: Future[WSResponse]) {
    def parse[R: Reads]: Future[R] = parse[R](RestException(_))

    def parse[R: Reads](customError: WSResponse => Throwable): Future[R] = response
      .filterWith(_.status == 200, customError)
      .transform(_.flatMap(_.json.asTry[R]))
  }

  protected implicit def serializeToJson[T: Writes](value: T): JsValue = toJson(value)

  protected sealed abstract class RequestType(val body: Option[JsValue]) {
    self: Product =>
    val method: String = self.productPrefix
  }

  protected case object GET extends RequestType(None)

  protected case object DELETE extends RequestType(None)

  protected case class POST(postBody: JsValue) extends RequestType(Some(postBody))

  protected case class PATCH(putBody: JsValue) extends RequestType(Some(putBody))

  protected case class PUT(putBody: JsValue) extends RequestType(Some(putBody))

}

object RestClient {
  private val maxPayloadLength = 20480
  private val cropedPayloadMsg = s"[croped at $maxPayloadLength chars long]"

  class RestException(val code: Int, msg: String, cause: Throwable = null) extends RuntimeException(msg, cause) {
    lazy val is404: Boolean = code == 404
    lazy val isUnathorized: Boolean = code == 401
    lazy val is4xx: Boolean = (400 to 499).contains(code)
    lazy val is5xx: Boolean = (500 to 599).contains(code)
  }

  object RestException{
    def apply(response: WSResponse): RestException = new RestException(response.status, s"${response.status} - ${response.statusText}: ${response.body.limit(maxPayloadLength, cropedPayloadMsg)}")
  }

  case class DetailedRestException(request: WSRequest, method: String, requestBody: Option[JsValue], response: WSResponse) extends RestException(response.status,
    s"""
       |  $method to '${request.url}'
       |    with headers ${request.headers}
       |    with body ${requestBody.map(_.toString.limit(maxPayloadLength, cropedPayloadMsg))}
       |
       |  Returned ${response.status}: ${response.statusText}
       |    with headers ${response.headers}
       |    with body ${response.body.limit(maxPayloadLength, cropedPayloadMsg)}
       |""".stripMargin
  )

}