package clients

import play.api.libs.json.{Format, Reads, Writes}
import utils.JsonWithDefaults.{format, reads, writes}

package object models {
  case class AuthRequest(apiKey: String)

  case class AuthResponse(token: String)

  case class ImagesCatalogResponse(pictures: List[ImageInfoLite], page: Int, pageCount: Int, hasMore: Boolean)

  case class ImageInfoLite(id: String, cropped_picture: String)

  case class ImageInfo(id: String,
                       author: Option[String],
                       camera: Option[String],
                       tags: Option[String],
                       cropped_picture: Option[String],
                       full_picture: String) {
    lazy val tagsList: List[String] = tags.toList.flatMap(_.split(" ").toList)
  }

  implicit val writesAuthRequest: Writes[AuthRequest] = writes[AuthRequest]
  implicit val readsAuthResponse: Reads[AuthResponse] = reads[AuthResponse]

  implicit val formatImageInfo: Format[ImageInfo] = format[ImageInfo]
  implicit val readsImageInfoLite: Reads[ImageInfoLite] = reads[ImageInfoLite]
  implicit val readsImagesCatalogResponse: Reads[ImagesCatalogResponse] = reads[ImagesCatalogResponse]
}