package controllers

import play.api.Logging
import play.api.libs.json.Json.toJson
import play.api.libs.json.Writes
import play.api.mvc.{Result, BaseController => PlayBaseController}

import scala.concurrent.{ExecutionContext, Future}

trait BaseController extends PlayBaseController with Logging {
  implicit val ec: ExecutionContext

  protected def returnValue[T: Writes](result: T): Result = Ok(toJson(result))

  protected def JsonActionAsync[T: Writes](responseBody: => Future[T]) = Action.async(responseBody.map(returnValue(_)))
}