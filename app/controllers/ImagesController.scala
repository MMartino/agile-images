package controllers

import javax.inject.Inject
import play.api.mvc.ControllerComponents
import service.ImagesService

import scala.concurrent.ExecutionContext

class ImagesController @Inject()(service: ImagesService,
                                 val controllerComponents: ControllerComponents)(implicit val ec: ExecutionContext) extends BaseController {
  def getAllImages() = JsonActionAsync(service.getAllImages())

  def searchImages(searchTerm: String) = JsonActionAsync(service.searchImages(searchTerm))

  def refreshCatalog() = JsonActionAsync(service.refreshCatalog())
}