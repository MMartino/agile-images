package controllers

import com.iheart.playSwagger.SwaggerSpecGenerator
import javax.inject.Inject
import play.api.libs.json.{JsObject, JsString}
import play.api.mvc._

import scala.util.Try

class MainController @Inject()(cc: ControllerComponents, assets: Assets) extends AbstractController(cc) {
  private lazy val swaggerSpec: Try[JsObject] = SwaggerSpecGenerator(swaggerV3 = false, "models", "controllers.model")(getClass.getClassLoader)
    .generate()
    .map(_ + ("host" -> JsString("localhost:8080")))

  def healthCheck() = Action(Ok("ok"))

  def getSwaggerFile() = Action {
    swaggerSpec.fold(error => InternalServerError(s"Couldn't generate swagger: ${error.getMessage}"), Ok(_))
  }

  def getSwaggerUIFile(file: String) = assets.at("/public/lib/swagger-ui", file)

  def redirectToSwaggerUI() = Action(Redirect("docs/swagger-ui/index.html?url=/assets/swagger.json"))
}