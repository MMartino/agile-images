package service

import clients.AgileClient
import clients.models.ImageInfo
import com.google.inject.Inject
import javax.inject.Singleton
import play.api.{Configuration, Logging}
import play.cache.AsyncCacheApi
import utils.extensions.CollectionExtensions.IterableExtensions

import scala.concurrent.{ExecutionContext, Future}
import scala.jdk.FutureConverters._

@Singleton
class ImagesService @Inject()(cache: AsyncCacheApi,
                              config: Configuration,
                              client: AgileClient)(implicit ec: ExecutionContext) extends Logging {
  private val catalogCacheKey = "catalog"
  private val byTagsCacheKey = "byTags"
  private val byAuthorCacheKey = "byAuthor"
  private val byCameraCacheKey = "byCamera"
  private val refreshPeriod: Int = config.get[Int]("agile.refresh.period.seconds")

  def getAllImages(): Future[List[String]] = catalog().map(_.values.map(_.full_picture).toList)

  def searchImages(searchTerm: String): Future[List[String]] = for {
    indexes <- Future.sequence(Seq(byAuthor(), byCamera(), byTags()))
    ids = indexes.flatMap(_.get(searchTerm).toList.flatten)
    catalog <- catalog()
    images = ids.flatMap(catalog.get).toList
  } yield images.map(_.full_picture)

  def refreshCatalog(): Future[List[ImageInfo]] = catalog().map(_.values.toList)

  private def catalog(): Future[Map[String, ImageInfo]] =
    cache.getOrElseUpdate[Map[String, ImageInfo]](catalogCacheKey, () => imagesCatalog().map(_.indexBy(_.id)).asJava, refreshPeriod).asScala

  private def byAuthor(): Future[Map[String, List[String]]] = cache
    .getOrElseUpdate[Map[String, List[String]]](byAuthorCacheKey, () => catalog().map(_
      .values.toList
      .filter(_.author.isDefined)
      .groupBy(_.author.get)
      .view.mapValues(_.map(_.id)).toMap
    ).asJava).asScala

  private def byCamera(): Future[Map[String, List[String]]] = cache
    .getOrElseUpdate[Map[String, List[String]]](byCameraCacheKey, () => catalog().map(_
      .values.toList
      .filter(_.camera.isDefined)
      .groupBy(_.camera.get)
      .view.mapValues(_.map(_.id)).toMap
    ).asJava).asScala

  private def byTags(): Future[Map[String, List[String]]] = cache
    .getOrElseUpdate[Map[String, List[String]]](byTagsCacheKey, () => catalog().map(_
      .values.toList
      .flatMap(i => i.tagsList.map(_ -> i.id))
      .groupBy(_._1)
      .view.mapValues(_.map(_._2)).toMap
    ).asJava).asScala

  private def imagesCatalog(): Future[List[ImageInfo]] = imagesCatalogIndex()
    .flatMap(ids => Future.sequence(ids.map(client.retrieveImageInfo))
    .map(_.flatten))

  private def imagesCatalogIndex(): Future[List[String]] = for {
    firstPage <- client.retrieveImagesCatalog()
    otherPages <- Future.sequence((2 to firstPage.pageCount).map(client.retrieveImagesCatalog(_)))
  } yield (firstPage.pictures ++ otherPages.flatMap(_.pictures)).map(_.id)
}

object ImagesService {

  import akka.actor._

  case object ReloadCatalog

  class CatalogRefresherActor @Inject()(service: ImagesService) extends Actor {
    override def preStart() = self ! ReloadCatalog

    override def receive = {
      case ReloadCatalog => service.refreshCatalog()
      case _ =>
    }
  }

}