package utils.conversions

import scala.concurrent.Future
import scala.util.{Success, Try}

object Lifting {

  implicit class LiftingOps[A](val value: A) extends AnyVal {
    /** Lifts the object's type to the specified Functor. */
    @inline def liftF[F[_]](implicit lifter: Lifter[F]): F[A] = lifter.lift(value)

    @inline def liftF[F1[_], F2[_]](implicit lifter1: Lifter[F1], lifter2: Lifter[F2]): F2[F1[A]] = lifter2.lift(lifter1.lift(value))
  }

  trait Lifter[F[_]] {
    def lift[A](value: A): F[A]
  }

  implicit object ListLifter extends Lifter[List] {
    @inline def lift[A](value: A): List[A] = List(value)
  }

  implicit object OptionLifter extends Lifter[Option] {
    @inline def lift[A](value: A): Option[A] = Option(value)
  }

  implicit object FutureLifter extends Lifter[Future] {
    @inline def lift[A](value: A): Future[A] = Future.successful(value)
  }

  implicit object TryLifter extends Lifter[Try] {
    @inline def lift[A](value: A): Try[A] = Success(value)
  }

}