package utils.extensions

import utils.noop

import scala.concurrent.{ExecutionContext, Future}
import scala.languageFeature.implicitConversions
import scala.util.{Failure, Success}

object AsyncExtensions {

  implicit class FutureOps[T](val f: Future[T]) extends AnyVal {
    /**
     * Do some operation with the result of this Future, returning this Future. Something like a fluent foreach.
     *
     * @tparam U Not used. Just to accept any returning type for `op`
     */
    @inline def perform[U](op: PartialFunction[T, U])(implicit ec: ExecutionContext): Future[T] = f.map(_.also(op.applyOrElse(_, noop)))

    /** Like [[Future.filter]], but using a custom [[Throwable]] in case the predicates is not satisfied. */
    @inline def filterWith(p: T => Boolean, customError: T => Throwable)(implicit ec: ExecutionContext): Future[T] =
      f.transform(_.flatMap(result => if (p(result)) Success(result) else Failure(customError(result))))

    /** If this [[Future]] is completed with an exception, apply `customError` to it and return a new [[Future]] completed with the resulting exception. */
    @inline def mapError(customError: PartialFunction[Throwable, Throwable])(implicit ec: ExecutionContext): Future[T] =
      f.transform(result => result.transform(_ => result, e => Failure(customError.applyOrElse(e, identity[Throwable]))))

    /** If this [[Future]] is completed with an exception, apply `logger` to it and return this [[Future]]. */
    @inline def logError(logger: PartialFunction[Throwable, Unit])(implicit ec: ExecutionContext): Future[T] =
      f.transform(_.also(_.failed.foreach(logger.applyOrElse(_, noop))))
  }

}