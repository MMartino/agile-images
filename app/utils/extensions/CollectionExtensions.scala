package utils.extensions

object CollectionExtensions {

  implicit class IterableExtensions[V](private val it: Iterable[V]) extends AnyVal {
    def indexBy[K](indexer: V => K): Map[K, V] = it.map(v => indexer(v) -> v).toMap
  }

}