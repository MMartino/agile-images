package utils.extensions

import play.api.libs.json.Json.parse
import play.api.libs.json._
import utils.extensions.StringExtensions.StringOps

import scala.util.{Failure, Success, Try}

object JsonExtensions {

  implicit class JsonStringExtensions(json: String) {
    /** Parses a String representing a JSON input, and returns it as a [[JsValue]]. */
    def toJsValue(): JsValue = parse(json.nullify.getOrElse("{}"))

    /** Parses a Json String into a Model case class, handling empty values and using custom Exception to improve error messages. */
    def parseJson[T: Reads]: T = toJsValue().as[T]

    /** Parses a Json String into a Model case class, handling empty values and using custom Exception to improve error messages. */
    def tryParseJson[T: Reads]: Try[T] = Try(toJsValue()).flatMap(_.asTry[T])
  }

  implicit class JsValueExtensions(json: JsValue) {
    /** Tries to convert the node into a T, returning a [[Failure]] with the exception if it can't. */
    def asTry[T: Reads]: Try[T] = json.validate[T].fold(valid = Success(_), invalid = e => Failure(JsResultException(e)))
  }

  implicit class ObjectToJson[T: Writes](o: T) {
    def toJson: JsValue = Json.toJson(o)

    def toJsonString: String = Json.toJson(o).toString()
  }

}