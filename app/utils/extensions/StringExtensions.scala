package utils.extensions

object StringExtensions {

  implicit class StringOps(val s: String) extends AnyVal {
    def nullify: Option[String] = if (s.isNullOrWhiteSpace) None else Some(s)

    /**
     * Determines if the string is null or white space.
     *
     * @return [[Boolean]] indicating whether the string is null or white space.
     */
    def isNullOrWhiteSpace: Boolean = s == null || s.isEmpty || s == "\"\"" || s.trim.isEmpty

    /** Return this string if length <= maxLength, otherwise return this string croped at maxLength with elipsis the specified suffix. */
    def limit(maxLength: Int, suffix: String = "") = if(s.length <= maxLength) s else s"${s.take(maxLength)}...$suffix"
  }

}