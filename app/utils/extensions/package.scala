package utils

package object extensions {

  /** Kotlin-like scoping functions. Useful for fluent-style method piping */
  implicit class ScopingFunctions[T](o: T) {
    /** Calls the specified function with this value as its argument and returns its result. */
    @inline def let[U](f: T => U): U = f(o)

    /** Calls the specified operation with this value as its argument and returns this value. */
    @inline def also[U](op: T => Unit): T = {
      op(o)
      o
    }
  }

}