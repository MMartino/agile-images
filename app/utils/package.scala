import play.api.libs.json.Json
import play.api.libs.json.Json.WithDefaultValues

package object utils {
  val JsonWithDefaults = Json.using[WithDefaultValues]

  def noop[T]: T => Unit = _ => {}
}