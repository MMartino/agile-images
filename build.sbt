import BaseProject.projectName

lazy val `agile-images` = Project(id = projectName, base = file("."))
  .enablePlugins(PlayScala, PackPlugin, SwaggerPlugin)
  .settings(settings: _*)

lazy val settings = Seq(
  BaseProject.settings,
  PlayConf.settings,
  Dependencies.settings,
  SbtPlugins.settings,
  Testing.settings
).flatten
