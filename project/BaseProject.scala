import sbt.Keys.{fork, _}
import sbt._

object BaseProject {
  val projectName = "agile-images"

  lazy val settings = Seq(
    version := "1.0.0",
    name := projectName,
    organization := "mmartino",

    scalaVersion := "2.13.4",
    crossVersion := CrossVersion.disabled,
    scalacOptions ++= Seq("-deprecation", "-feature"),

    fork in run := false
  )
}