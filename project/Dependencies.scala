import play.sbt.PlayImport.{caffeine, guice, ws}
import sbt.Append.appendSeq
import sbt.Keys._
import sbt._

object Dependencies {
  private val akkaVersion = "2.5.31"
  private val akkaHttpVersion = "10.1.12"

  lazy val settings = Seq(
    //Dependencies
    libraryDependencies ++= (runtime ++ test),
    dependencyOverrides ++= conflicting
  )

  lazy val runtime = Seq(
    ws, guice, caffeine,

    "org.typelevel" %% "cats-core" % "2.0.0",

    "org.webjars" % "swagger-ui" % "3.35.0",
    "com.iheart" %% "play-swagger" % "0.10.2"
  )

  lazy val conflicting = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-slf4j" % akkaVersion,
    "com.typesafe.akka" %% "akka-stream" % akkaVersion,
    "com.typesafe.akka" %% "akka-http" % akkaHttpVersion,
    "com.typesafe.akka" %% "akka-protobuf" % akkaVersion
  )

  private lazy val test = Seq(
    "org.mockito" % "mockito-core" % "3.7.7",
    "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3"
  ).map(_ % Test)
}