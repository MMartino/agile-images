import play.sbt.routes.RoutesKeys.{InjectedRoutesGenerator, routesGenerator}

object PlayConf {
  lazy val settings = Seq(
    routesGenerator := InjectedRoutesGenerator
  )
}