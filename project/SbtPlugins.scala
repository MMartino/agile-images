import com.iheart.sbtPlaySwagger.SwaggerPlugin.autoImport.swaggerDomainNameSpaces
import com.typesafe.sbt.SbtNativePackager.autoImport.packageName
import com.typesafe.sbt.packager.universal.UniversalPlugin.autoImport.Universal
import xerial.sbt.pack.PackPlugin.autoImport.{packGenerateWindowsBatFile, packMain}

object SbtPlugins {
  lazy val settings = Seq(
    packGenerateWindowsBatFile := false,
    packageName in Universal := s"agile_images_dist",
    packMain := Map("agile-images" -> "play.core.server.ProdServerStart"),

    swaggerDomainNameSpaces := Seq("models", "controllers.model")
  )
}