import sbt.Append.appendSeq
import sbt.Keys._
import sbt.{Tags, Test, _}

object Testing {
  lazy val settings = testSettings ++ Seq(concurrentRestrictions in Global += Tags.limit(Tags.Test, 1))

  private lazy val testSettings = inConfig(Test)(Seq(
    fork := true,
    parallelExecution := false
  ))
}