addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.7.9")
addSbtPlugin("com.iheart" % "sbt-play-swagger" % "0.10.2")

addSbtPlugin("org.xerial.sbt" % "sbt-pack" % "0.12")